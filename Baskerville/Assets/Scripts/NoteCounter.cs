using System;
using UnityEngine;
using TMPro;

public class NoteCounter : MonoBehaviour
{
    public int counter = 0;

    public TextMeshProUGUI counterText;

    public Animator panel;

    public Canvas mainCanvas;

    public Canvas endCanvas;

    public float timeRemaining = 15;

    public bool counterFinished = false;

    public float endTimer = 8;

    public AudioClip dog;
    
    public AudioClip watson;

    public bool canBePlayed = true;

    public bool dogPlayed = false;

    public bool watsonPlayed = false;

    private void Start()
    {
        panel.enabled = false;
        endCanvas.enabled = false;
    }

    private void Update()
    {
        counterText.text = counter + "/13";

        if (counter == 13)
        {
            if (timeRemaining > 0 && !counterFinished )
            {
                timeRemaining -= Time.deltaTime;
                endCanvas.enabled = true;
            }
        }

        if (timeRemaining <= 0)
        {
            counterFinished = true;
            mainCanvas.enabled = false;
            panel.enabled = true;
            
            if (endTimer > 0 && !dogPlayed && !watsonPlayed)
            {
                endTimer -= Time.deltaTime;
                SoundManager.instance.PlaySound(dog);
                SoundManager.instance.PlaySound(watson);
                dogPlayed = true;
                watsonPlayed = true;
            }
        }
        
        

        if (endTimer <= 0 && canBePlayed)
        {
            canBePlayed = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        

    }
}
