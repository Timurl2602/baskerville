using UnityEngine.UI;
using UnityEngine;
using TMPro;


public class NewInteraction : MonoBehaviour
{
    [SerializeField] 
    private Image _noteImage;
    public Image noteImage
    {
        get { return _noteImage; }
    }

    [SerializeField]
    private TextMeshProUGUI interactionText;

    public bool isInRange = false;
    public bool isNoteOpen = false;

    public NoteCounter noteCounter;

    private bool isCounterUpdated = false;


    private void Awake()
    {
        noteImage.gameObject.SetActive(false);
        interactionText.gameObject.SetActive(false);
    }

    private void Update()
    {

        if (isInRange)  
        {

            if (Input.GetKeyDown(KeyCode.F))
            {
                noteImage.gameObject.SetActive(!noteImage.gameObject.activeSelf);
            } 
        }
        else
        {
            noteImage.gameObject.SetActive(false);
        }

        if (noteImage.gameObject.activeSelf == true)
        {
            isNoteOpen = true;

            if (!isCounterUpdated)
            {
                noteCounter.counter++;
                isCounterUpdated = true;
            }
            
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                noteImage.gameObject.SetActive(false);
                isNoteOpen = false;
            }
        }
        else
        {
            isNoteOpen = false;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.gameObject.SetActive(true);
            isInRange = true;
            Debug.Log("Player entered");
        }

    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.gameObject.SetActive(false);
            isInRange = false;
            Debug.Log("Player left");
        }
    }
}