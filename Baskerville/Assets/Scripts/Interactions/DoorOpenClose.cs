using UnityEngine;

public class DoorOpenClose : MonoBehaviour
{
    [SerializeField]
    private GameObject doorOpen;

    [SerializeField]
    private GameObject doorClose;

    [SerializeField]
    private NewInteraction interaction;
    
    [SerializeField]
    private AudioClip doorSound;


    private bool isPlayable = false;

    private bool alreadyPlayed = false;

    private void Update()
    {
        if (interaction.isNoteOpen)
        {
            doorOpen.gameObject.SetActive(true);
            isPlayable = true;
            doorClose.gameObject.SetActive(false);
        }

        if (interaction.isNoteOpen == false && isPlayable && alreadyPlayed == false)
        {
            SoundManager.instance.PlaySound(doorSound);
            isPlayable = false;
            alreadyPlayed = true;
        }
        
    }
}
