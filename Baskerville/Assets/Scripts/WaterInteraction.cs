﻿using UnityEngine;

namespace DefaultNamespace
{
    public class WaterInteraction : MonoBehaviour
    {
       [SerializeField] private PlayerMovement player;
        
        private void OnTriggerEnter(Collider collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                player.speed = 3f;
                Debug.Log("Splish Splash");
            }
        
        }
    
         private void OnTriggerExit(Collider collision)
          {
            if (collision.gameObject.CompareTag("Player"))
            {
                player.speed = 10f;
                Debug.Log("Towel please!");
            }
          }
         
        
    }
}