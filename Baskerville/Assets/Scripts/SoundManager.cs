using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    [SerializeField] private AudioClip gameplayMusic;

    [SerializeField] private AudioSource musicSource, effectsSource;

    private bool canBePlayed = true;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
        AudioListener.volume = PlayerPrefs.GetFloat("MasterVolume", 1);
    }

    private void Update()
    {
        var currentScene = SceneManager.GetActiveScene();

        if (canBePlayed)
        {
            if (currentScene.name == "Gameplay")
            {
                musicSource.Stop();
                instance.PlayMusic(gameplayMusic);
                canBePlayed = false;
            }
        }
    }

    public void PlaySound(AudioClip clip)
    {
        effectsSource.PlayOneShot(clip);
    }
    
    public void PlayMusic(AudioClip clip)
    {
        musicSource.PlayOneShot(clip);
    }
}