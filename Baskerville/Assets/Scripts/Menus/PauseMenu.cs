using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
        [SerializeField]
        private GameObject pauseMenu;
        
        public bool IsPaused;

        private NewInteraction interactionsystem;

        
        private void Start() {
            pauseMenu.SetActive(false);
        }
        
        private void Update() 
        {
            if (Input.GetKeyDown(KeyCode.Escape)) 
            {
                if (IsPaused) 
                {
                    ResumeGame();
                } else 
                {
                    PauseGame();
                }
            }
        }

        // stop game and show pause menu
        public void PauseGame()
        {
            Cursor.lockState = CursorLockMode.None;
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
            IsPaused = true;
        }

        // if resumegame clicked, start playing game again and set pause menu on disabled
        public void ResumeGame()
        {
            Cursor.lockState = CursorLockMode.Locked;
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
            IsPaused = false;
        }

        // go back to main menu through pause menu
        public void GoToMainMenu() 
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 1f;
            IsPaused = false;
            SceneManager.LoadScene("MainMenu");
        }

        // quit game
        public void QuitGame() 
        {
            Application.Quit();
        }
}
