using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] private float mouseSensitivity;

    [SerializeField] private Transform playerBody;
    
    private float xRotation = 0f;

    public PauseMenu pauseMenu;

    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    
    }

    
    void Update()
    {
        if (!pauseMenu.IsPaused)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        
            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerBody.Rotate(Vector3.up * mouseX);
        }
        
    }
}
