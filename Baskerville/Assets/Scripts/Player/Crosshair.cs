using UnityEngine;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour
{


    [SerializeField] private Camera mainCamera;
    
    [SerializeField] private Image crosshairStyle;

    [SerializeField] private float maxDistance;

    [SerializeField] private Color color;

    [SerializeField] private Color raycastColor;




    private void Update()
    {
        int layerMask = LayerMask.GetMask("Interactable");
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        

        if (Physics.Raycast(ray, out hit, maxDistance, layerMask))
        {
            crosshairStyle.color = color;
        }
        else
        {
            crosshairStyle.color = raycastColor;
        }
    }
}
