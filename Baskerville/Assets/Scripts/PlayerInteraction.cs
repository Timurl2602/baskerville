using System;
using UnityEngine;
using TMPro;

public class PlayerInteraction : MonoBehaviour
{
    [SerializeField]
    private TMP_Text playerText;
    public string currentInteractableText;
    public bool isTimerRunning = false;
    [SerializeField]
    public float timerLength;
    public GameObject textBox;

    private void Start()
    {
        textBox.SetActive(false);
    }

    void Update()
    {

        if (isTimerRunning)
        {
            if (timerLength > 0)
            {
                playerText.text = currentInteractableText;
                timerLength -= Time.deltaTime;
            }
            else
            {
                isTimerRunning = false;
            }
        }
        else
        {
            playerText.text = "";
            textBox.SetActive(false);
        }
    }
    
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("PlayerInteractable"))
        {
            if (!isTimerRunning)
            {
                textBox.SetActive(true);
                isTimerRunning = true;
                currentInteractableText = collision.gameObject.GetComponent<Interactable>().myText.text;
                collision.gameObject.SetActive(false);
                timerLength = collision.gameObject.GetComponent<TimerLength>().length;
            }

        }
        
    }
    
    
}
