using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/InteractableText", order = 1)]
public class InteractScriptableObject : ScriptableObject
{
    public string text;
}
