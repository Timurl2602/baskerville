﻿using UnityEngine;

namespace DefaultNamespace
{
    public class SoundInteraction : MonoBehaviour
    {
        [SerializeField] private AudioClip clip;
        [SerializeField] private AudioSource source;
        [SerializeField] private bool alreadyPlayed = false;
        private void OnTriggerEnter(Collider collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (!alreadyPlayed)
                {
                    source.PlayOneShot(clip);
                    alreadyPlayed = true;
                }

            }
        
        }
        
        
    }
}